# OpenML dataset: porto-seguro

https://www.openml.org/d/42206

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Training dataset of the 'Porto Seguros Safe Driver Prediction' Kaggle challenge [https://www.kaggle.com/c/porto-seguro-safe-driver-prediction]. The goal was to predict whether a driver will file an insurance claim next year. The official rules of the challenge explicitely state that the data may be used for 'academic research and education, and other non-commercial purposes' [https://www.kaggle.com/c/porto-seguro-safe-driver-prediction/rules]. For a description of all variables checkout the Kaggle dataset repository [https://www.kaggle.com/c/porto-seguro-safe-driver-prediction/data]. It states that numeric features with integer values that do not contain 'bin' or 'cat' in their variable names are in fact ordinal features which could be treated as ordinal factors in R. For further information on effective preprocessing and feature engineering checkout the 'Kernels' section of the Kaggle challenge website [https://www.kaggle.com/c/porto-seguro-safe-driver-prediction/kernels]. For this version we removed all 'calc' variables, as the Kaggle forum indicates that they do not carry much information.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42206) of an [OpenML dataset](https://www.openml.org/d/42206). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42206/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42206/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42206/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

